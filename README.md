# QR Code Generator

## Project Details

- [ ] Written in Structured Text
- [ ] Codesys Version 3.5.19.20
- [ ] Target: Edge Controller 752-8303/8000-002 FW26
- [ ] Includes Visualization

## Name
QR Code Generator for Codesys Visualizations

![Program](visuExample.png)

## Description
This repo uses qrencode running in a Docker Container to generate QR images, based on a string that it is given. This can then be saved in a location where it can be accessed within CODESYS. Dynamic bitmap can be used to dynamically update the image in CODESYS

Pull Docker Image

Enable or Install Docker on your device

docker pull mpsaltis/wago_qrencode

In the terminal, the following command can then be used to create QR code for www.google.com that is saved in the /home directory.

```
docker run --rm -t -v /home/:/tmp mpsaltis/wago_qrencode qrencode -l L -o /tmp/placeholderqr.bmp www.google.com
```

Note: The directory may be changed to /home/codesys_root/PlcLogic/visu to be used with an image pool in Codesys.
Dynamically update the QR Code with Codesys v3.5

First run the following command in the shell of the PLC to generate a PNG QR code with the name palceholderqr in the home directory:

```
docker run --rm -t -v /home/:/tmp mpsaltis/wago_qrencode qrencode -l L -o /tmp/placeholderqr.bmp www.google.com
```

Then move this image from the PLC, to your PC to be later imported into Codesys. This can be done with a utility like WinSCP

Create an image pool in Codesys, and add the .png as embedded.

Note- The image must be a PNG to be updated dynamically

![Program](codesysExample1.png)

Create a POU with an FUExecuteCommand function block to pass the command to the shell from Codesys.
```
PROGRAM PLC_PRG
VAR
xExecute: BOOL;
bSelect: BYTE;
sCommand: STRING(1024);
R_sStdOut: STRING;
R_sStdError: STRING;
pResult: WagoSysProcess.SysTypes.RTS_IEC_RESULT;
sURL: STRING(128) := 'www.wago.com';
xUpdateImage: INT;
END_VAR
```
```
sURL;
sCommand := CONCAT('docker run --rm -t -v /home/codesys_root/PlcLogic/visu:/tmp mpsaltis/wago_qrencode qrencode -l L -o /tmp/placeholderqr.bmp ', sUrl); 


IF xExecute THEN	
WagoSysProcess.FuExecuteCommand(
sCommand:=sCommand , 
R_sStdOut:=R_sStdOut , 
uiStdOutSize:=80 , 
R_sStdError:=R_sStdError , 
uiStdErrorSize:=80 , 
tTimeout:=T#5S , 
pResult:=ADR(pResult) );
xExecute:=FALSE;
xUpdateImage:=xUpdateImage+1;
END_IF

sURL : variable for the URL address. xUpdateImage : Used by the visualization to update the image when the value changes.
```

![Program](codesysExample2.png)

Add the image to a visualization, and assign the Bitmap ID variable as the name of the ImagePool bitmap. Set the Bitmap Version to a variable that is updated when a new QR code is generated.

![Program](codesysExample3.png)



## Support
This program is for demonstration only and does not include support.  May contain bugs.  Use at your own risk.

## Authors and acknowledgment
Michael Psaltis
May 5 2024

## License
Copyright (c) 2024, Michael Psaltis, WAGO

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE
